package configs;

import socket.Server;

import java.io.InputStream;
import java.util.Properties;

public class Configs {
    // параметры необходимы для подтягивания конфигурации во всем проекте
    public static InputStream input = Server.class.getClassLoader().getResourceAsStream("config.properties");
    public static Properties property = new Properties();
}
