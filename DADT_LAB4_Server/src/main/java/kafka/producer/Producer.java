package kafka.producer;


import org.apache.kafka.common.errors.SaslAuthenticationException;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.log4j.Logger;
import kafka.producer.kafkaconnectionproducer.KafkaConnection;
import org.json.JSONObject;

import java.sql.SQLException;

public class Producer {
    static final Logger logger = Logger.getLogger(Producer.class);
    public static void run (JSONObject jsonObject) {
        try {
            logger.info("Подготовка данных для отправки в Kafka...");
            KafkaConnection.SendData(jsonObject);
        }
        catch(NullPointerException | SQLException e) {
            logger.error(String.format("Данные не были отправлены в Kafka,%s", e.getMessage()));
        }
        catch (SaslAuthenticationException authEx) {
            logger.error(String.format("Ошибка аутентификации в Kafka: %s", authEx.getMessage()));
        } catch (SerializationException serEx){
            logger.error(String.format("Ошибка десериализации данных: %s", serEx.getMessage()));
        }
    }
}
