package kafka.producer.configs;

import configs.Configs;
import lombok.Getter;

public enum MainConfig {

    CONFIG;

    private @Getter int tablePollIntervalSeconds;
    private @Getter int tableDelayIntervalSeconds;

    MainConfig() {
        try {
            tablePollIntervalSeconds = Integer.parseInt(Configs.property.getProperty("kafka.table.poll.interval.seconds"));
            tableDelayIntervalSeconds = Integer.parseInt(Configs.property.getProperty("kafka.table.delay.interval.seconds"));
        } catch (Exception e) {
            System.out.println(String.format("Ошибка загрузки конфигурации. Текст ошибки: %s", e.getMessage()));
        }
    }
}
