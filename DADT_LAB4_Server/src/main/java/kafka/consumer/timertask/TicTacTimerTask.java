package kafka.consumer.timertask;

import kafka.consumer.Consumer;
import kafka.consumer.configs.KafkaConnectionConfig;
import kafka.consumer.tictacconnection.TicTacConnection;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.SaslAuthenticationException;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.log4j.Logger;
import tictactoe_schema.avro.tictactoe;

import java.time.Duration;
import java.util.Arrays;
import java.util.TimerTask;

import static kafka.consumer.kafkaconnectionproducer.KafkaConnection.createConnection;

public class TicTacTimerTask extends TimerTask {
    static final Logger logger = Logger.getLogger(Consumer.class);

    @Override
    public void run() {
        boolean sendingStatus = false;
        try (KafkaConsumer<String, tictactoe> consumer = createConnection()) {
            //logger.info("Подписка на топик " + KafkaConnectionConfig.CONFIG.getKafkaTopicName() + " с консьюмер-группой " + KafkaConnectionConfig.CONFIG.getConsumerGroup());
            consumer.subscribe(Arrays.asList(KafkaConnectionConfig.CONFIG.getKafkaTopicName()));
            //logger.info("Подписка на топик выполнена успешно");

            //logger.info("Выполнение poll-запроса для получения данных из Kafka");
            ConsumerRecords<String, tictactoe> consumerRecords = consumer.poll(Duration.ofSeconds(KafkaConnectionConfig.CONFIG.getPollTimeoutSeconds()));
            //logger.info("Poll-запрос выполнен успешно");

            if (!consumerRecords.isEmpty()) {
                logger.info("Вычитано " + consumerRecords.count() + " сообщений");
                sendingStatus = TicTacConnection.SendData(consumerRecords);
                for (ConsumerRecord<String, tictactoe> acc: consumerRecords) {
                    System.out.println(acc.value());
                }
            } else {
                logger.info("В топике Kafka нет новых сообщений для текущей консьюмер-группы " + KafkaConnectionConfig.CONFIG.getConsumerGroup());
            }
            if (sendingStatus) {
                //logger.info("Выполнение commit offset'а в Kafka");
                consumer.commitSync();
                logger.info("Сommit offset'а выполнен успешно.");
            }
        }
        catch (SaslAuthenticationException authEx) {
            logger.error(String.format("Ошибка аутентификации в Kafka: %s", authEx.getMessage()));
        }
        catch (SerializationException serEx){
            logger.error(String.format("Ошибка десериализации данных: %s", serEx.getMessage()));
        }
    }
}
