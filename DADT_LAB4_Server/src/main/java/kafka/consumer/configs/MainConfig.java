package kafka.consumer.configs;

import configs.Configs;
import kafka.consumer.kafkaconnectionproducer.KafkaConnection;
import lombok.Getter;
import org.apache.log4j.Logger;

public enum MainConfig {

    CONFIG;

    private @Getter int kafkaConsumeIntervalSeconds;
    private @Getter int kafkaDelayIntervalSeconds;

    MainConfig() {
        Logger logger = Logger.getLogger(KafkaConnection.class);
        try {
            kafkaConsumeIntervalSeconds = Integer.parseInt(Configs.property.getProperty("kafka.consume.interval.seconds"));
            kafkaDelayIntervalSeconds = Integer.parseInt(Configs.property.getProperty("kafka.delay.interval.seconds"));
        } catch (Exception e) {
            logger.error("Ошибка загрузки Main-конфигурации. Текст ошибки: " + e.getMessage());
        }
    }
}
