package kafka.consumer.kafkaconnectionproducer;

import kafka.consumer.configs.KafkaConnectionConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.log4j.Logger;
import tictactoe_schema.avro.tictactoe;

public class KafkaConnection {

    static Logger logger = Logger.getLogger(KafkaConnection.class);

    public static KafkaConsumer<String, tictactoe> createConnection() {
        KafkaConsumer<String, tictactoe>  newKafkaConnection = null;
        try {
            logger.info("Подключение к Kafka...");
            newKafkaConnection = new KafkaConsumer<>(KafkaConnectionConfig.CONFIG.getConsumerProperties());
        } catch (Exception e) {
            logger.error("Ошибка подключения к Kafka" + e.getMessage());
            e.printStackTrace();
        }
        return newKafkaConnection;
    }
}
