package kafka.consumer.tictacconnection;

import configs.Configs;
import dto.*;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import tictactoe_schema.avro.tictactoe;

import java.util.Objects;

import static socket.ServerService.userNameList;
import static socket.ServerService.currentRooms;
import static socket.ServerService.roomList;

public class TicTacConnection {
    public static boolean SendData(ConsumerRecords<String, tictactoe> consumerRecords) {
        for (ConsumerRecord<String, tictactoe> consumerRecord : consumerRecords) {
            // Если вычитанное сообщение не принадлежит серверу,
            // то тогда применяем изменения
            // иначе ничего не делаем
            if (consumerRecord.value().getIdServer() != Integer.parseInt(Configs.property.getProperty("kafka.id.server"))) {
                CharSequence typeRequest = consumerRecord.value().getTypeRequest();
                // обработка занятых имен пользователей
                if ("take_username".contentEquals(typeRequest)) {
                    String username = consumerRecord.value().getUsername().toString();
                    if (!userNameList.contains(username)) {
                        userNameList.add(username);
                    }
                    for (String user : userNameList) {
                        System.out.println(user);
                    }
                // обработка создания комнаты
                } else if ("create_room".contentEquals(typeRequest)) {
                    String username = consumerRecord.value().getUsername().toString();
                    Integer roomNumber = consumerRecord.value().getIdRoom();
                    currentRooms++;

                    RoomDTO roomDTO = new RoomDTO();
                    roomDTO.setRoomId(roomNumber);
                    roomDTO.setUserName1(username);
                    roomDTO.setServerController1(null);
                    roomDTO.setUserName2(null);
                    roomDTO.setTicTacToeArray(new Integer[3][3]);
                    for (int i = 0; i < 3; i++) {
                        for (int j = 0; j < 3; j++) {
                            roomDTO.getTicTacToeArray()[i][j] = 0;
                            System.out.println(roomDTO.getTicTacToeArray()[i][j]);
                        }
                    }
                    roomList.add(roomDTO);
                // обработка подключений к комнате
                } else if ("connect_to_the_room".contentEquals(typeRequest)) {
                    String username = consumerRecord.value().getUsername().toString();
                    Integer roomId = consumerRecord.value().getIdRoom();
                    for (RoomDTO room : roomList) {
                        if (Objects.equals(room.getRoomId(), roomId)) {
                            room.setUserName2(username);
                            room.setServerController2(null);
                        }
                    }
                // обработка занятия клеточек в определенной комнате
                } else if ("take_the_cell".contentEquals(typeRequest)) {
                    Integer idRoom = consumerRecord.value().getIdRoom();
                    String username = consumerRecord.value().getUsername().toString();
                    Integer cellNumber = consumerRecord.value().getIdCell();
                    int j = cellNumber % 3;
                    int i = cellNumber / 3;

                    for (RoomDTO roomDTO : roomList) {
                        if (Objects.equals(roomDTO.getRoomId(), idRoom)){
                            if (Objects.equals(roomDTO.getUserName1(), username)) {
                                roomDTO.getTicTacToeArray()[i][j] = 1;
                            } else {
                                if (Objects.equals(roomDTO.getUserName2(), username)) {
                                    roomDTO.getTicTacToeArray()[i][j] = 2;
                                }
                            }
                            for (int q = 0; q < 3; q++) {
                                for (int w = 0; w < 3; w++) {
                                    System.out.print(roomDTO.getTicTacToeArray()[q][w] + " ");
                                }
                                System.out.println();
                            }
                        }
                    }
                // обработка отключения от комнаты
                } else if ("disconnect_from_the_room".contentEquals(typeRequest)) {
                    String username = consumerRecord.value().getUsername().toString();
                    Integer roomId = consumerRecord.value().getIdRoom();
                    DisconnectRoomDTO disconnectRoom = new DisconnectRoomDTO();
                    disconnectRoom.setUserName(username);
                    disconnectRoom.setIdRoom(roomId);
                    disconnectRoom.setDisconnectStatus("failed");

                    for (RoomDTO room : roomList) {
                        if (Objects.equals(room.getRoomId(), roomId)) {
                            if (Objects.equals(room.getUserName1(), username)) {
                                room.setUserName1(null);
                            }
                            if (Objects.equals(room.getUserName2(), username)) {
                                room.setUserName2(null);
                            }
                        }
                    }
                    synchronized (roomList) {
                        roomList.removeIf(roomDTO -> (roomDTO.getUserName1() == null && roomDTO.getUserName2() == null));
                    }
                }
            }
        }
        return true;
    }
}
