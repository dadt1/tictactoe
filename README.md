# DADT_lab4



## Лабораторная работа №4. Разработка распределенного приложения для локальной сети.

Цель: изучение методов коммуникации процессов в сети, а также средств динамического конфигурирования распределенных приложений.
Проверяемые компетенции: способность работы с информацией из различных источников, включая сетевые ресурсы сети Интернет, для решения профессиональных задач; способность применять на практике теоретические основы и общие принципы разработки распределенных систем; уверенное знание теоретических и практических основ построения распределенных баз данных; способность использовать на практике стандарты сетевого взаимодействия компонент распределенной системы.

Задание выполняется в группе (не более трех студентов) или индивидуально. Каждый студент отчитывается по каждому пункту задания индивидуально.

Требования к выполнению работы:

Приложение должно обеспечивать параллельную работу нескольких клиентов и серверов. Дополнительное требование: возможность запуска нескольких серверов на одном компьютере.

Клиентские приложения должны автоматически находить серверы для обслуживания и выполнения заданных функций.

Серверы системы могут выполнять различные функции.

При разрыве сеанса приложения должны автоматически восстанавливать свою работоспособность.

Приложения должны поддерживать возможность взаимодействия в различных режимах.

Для организации взаимодействия нужно использовать различные средства коммуникации (именованные каналы, мейлслоты, сокеты, очереди сообщений, удалённый вызов процедур, WCF-сервисы), сравнив их возможности.

По окончании выполнения задания каждая группа студентов должна подготовить отчет.
    
Отчет по выполнению задания должен включать:
1.	Общее описание приложения. Постановка задачи, введение в предметную область.
2.	Архитектура системы. Обоснование выбора данного типа архитектуры распределенного приложения. Алгоритм работы приложения в целом.
3.	Архитектура каждого из логических компонент системы (серверы, клиенты, диспетчеры). Подходы к реализации. Алгоритмы работы. Многопоточность, обоснование.
4.	Методы коммуникаций компонентов системы (клиент→сервер, сервер→клиент и т. д.). Обоснование выбора этих методов коммуникации.
5.	Способ передачи данных (синхронная / асинхронная, однонаправленная / двунаправленная и т. д.). Обоснование.
6.	Структура передаваемых данных. Вид протоколов, обоснование выбора.
7.	Отказоустойчивость системы. Как система поведет себя, если «исчезнет» один или несколько ее компонент. Что произойдет с системой, если «исчезнувший» компонент будет восстановлен на другом узле сети.
8.	Работа с базой данных (если используется). Обоснование.
9.	Исходный код приложений с комментариями.

